package com.example.servicemodule.service;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.os.RemoteException;
import android.support.annotation.Nullable;

import com.example.servicemodule.IBinderPool;
import com.example.servicemodule.aidlimpl.ComputeImpl;
import com.example.servicemodule.aidlimpl.SecurityCenterImpl;

/**
 *  描述: 用于IPC的Binder连接池服务，通过binderCode来区分实现方式
 *  @Author: kehua.ding
 *  @Maintainer: kehua.ding
 *  @Date: 2018/11/27
 *  @Copyright: 2018 www.xgimi.com Inc. All rights reserved.
 */
public class BinderPoolService extends Service {

    public static final int BINDER_NONE = -1;

    public static final int BINDER_COMPUTE = 0;

    public static final int BINDER_SECURITY_CENTER = 1;

    private Binder mBinderPool = new IBinderPool.Stub() {
        @Override
        public IBinder queryBinder(int binderCode) throws RemoteException {
            IBinder binder = null;
            switch (binderCode) {
                case BINDER_SECURITY_CENTER: {
                    binder = new SecurityCenterImpl();
                    break;
                }
                case BINDER_COMPUTE: {
                    binder = new ComputeImpl();
                    break;
                }
                case BINDER_NONE: {
                    break;
                }
                default:
                    break;
            }
            return binder;
        }
    };

    @Override
    public IBinder onBind(@Nullable Intent intent) {
        return mBinderPool;
    }
}

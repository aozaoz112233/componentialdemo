package com.example.servicemodule.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.example.servicemodule.R;
/**
 *  描述: 待完成
 *  @Author: kehua.ding
 *  @Maintainer: kehua.ding
 *  @Date: 2018/11/27
 *  @Copyright: 2018 www.xgimi.com Inc. All rights reserved.
 */
public class ServiceActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_service);
    }
}

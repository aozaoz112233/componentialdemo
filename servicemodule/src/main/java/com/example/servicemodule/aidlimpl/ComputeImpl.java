package com.example.servicemodule.aidlimpl;

import android.os.RemoteException;

import com.example.servicemodule.ICompute;
/**
 *  描述: 简单的加法接口实现
 *  @Author: kehua.ding
 *  @Maintainer: kehua.ding
 *  @Date: 2018/11/27
 *  @Copyright: 2018 www.xgimi.com Inc. All rights reserved.
 */
public class ComputeImpl extends ICompute.Stub {
    @Override
    public int add(int a, int b) throws RemoteException {
        return a + b;
    }
}

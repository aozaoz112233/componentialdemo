package com.example.servicemodule.aidlimpl;

import android.os.RemoteException;
import android.support.annotation.NonNull;

import com.example.servicemodule.ISecurityCenter;
/**
 *  描述: 简单的加解密接口实现
 *  @Author: kehua.ding
 *  @Maintainer: kehua.ding
 *  @Date: 2018/11/27
 *  @Copyright: 2018 www.xgimi.com Inc. All rights reserved.
 */
public class SecurityCenterImpl extends ISecurityCenter.Stub {
    private static final char SECRET_CODE = '^';
    @Override
    public String encrypt(@NonNull String content) throws RemoteException {
        char [] chars = content.toCharArray();
        for (int i = 0; i < chars.length; i++){
            chars[i] ^= SECRET_CODE;
        }
        return new String(chars);
    }

    @Override
    public String decrypt(@NonNull String password) throws RemoteException {
        return encrypt(password);
    }
}

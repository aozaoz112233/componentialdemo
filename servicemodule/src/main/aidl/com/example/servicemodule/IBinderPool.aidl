// IBinderPool.aidl
package com.example.servicemodule;

interface IBinderPool {
     IBinder queryBinder(int binderCode);
}

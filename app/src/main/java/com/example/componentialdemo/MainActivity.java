package com.example.componentialdemo;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.os.Bundle;
import android.support.v7.widget.AppCompatButton;
import android.view.View;

import com.alibaba.android.arouter.launcher.ARouter;

/**
 * 描述: 测试ARoute简单的Navigatie
 *
 * @Author: kehua.ding
 * @Maintainer: kehua.ding
 * @Date: 2018/11/27
 * @Copyright: 2018 www.xgimi.com Inc. All rights reserved.
 */
public class MainActivity extends Activity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initUi();
    }

    private void initUi() {
        AppCompatButton buttonService = findViewById(R.id.button_navigate_service);
        AppCompatButton buttonClient = findViewById(R.id.button_navigate_client);
        buttonService.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(@NonNull View v) {
                ARouter.getInstance()
                       .build("/servicemodule/serviceActivity")
                       .navigation();
            }
        });
        buttonClient.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(@NonNull View v) {
                ARouter.getInstance()
                       .build("/clientmodule/clientActivity")
                       .navigation();
            }
        });
    }
}

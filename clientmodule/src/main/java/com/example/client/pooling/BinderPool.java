package com.example.client.pooling;

import android.annotation.SuppressLint;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.os.RemoteException;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.example.servicemodule.IBinderPool;

import java.util.concurrent.CountDownLatch;
/**
 *  描述: Binder连接池
 *  @Author: kehua.ding
 *  @Maintainer: kehua.ding
 *  @Date: 2018/11/27
 *  @Copyright: 2018 www.xgimi.com Inc. All rights reserved.
 */
public class BinderPool {

    private Context mContext;

    private IBinderPool mBinderPool;

    /**
     * ApplicationContext不会造成leak
     */
    @SuppressLint("StaticFieldLeak")
    private static volatile BinderPool sInstance;

    private CountDownLatch mConnectBinderPoolCountDownLatch;

    private static final String SERVICE_PACKAGENAME = "com.example.servicemodule";

    private static final String SERVICE_CLASS = "com.example.servicemodule.service.BinderPoolService";

    private BinderPool(@NonNull Context context) {
        mContext = context.getApplicationContext();
        connectBinderPoolService();
    }

    public static BinderPool getInstance(@NonNull Context context) {
        if (sInstance == null) {
            synchronized (BinderPool.class) {
                if (sInstance == null) {
                    sInstance = new BinderPool(context);
                }
            }
        }
        return sInstance;
    }

    private synchronized void connectBinderPoolService() {
        mConnectBinderPoolCountDownLatch = new CountDownLatch(1);
        Intent service = new Intent();
        service.setComponent(new ComponentName(SERVICE_PACKAGENAME,SERVICE_CLASS));
        mContext.bindService(service,mBinderPoolServiceConnection,Context.BIND_AUTO_CREATE);
        try {
            mConnectBinderPoolCountDownLatch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public IBinder queryBinder(int binderCode) {
        IBinder binder = null;
        try {
            if (mBinderPool != null) {
                binder = mBinderPool.queryBinder(binderCode);
            }
        } catch (RemoteException e) {
            e.printStackTrace();
        }
        return binder;
    }

    private ServiceConnection mBinderPoolServiceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(@NonNull ComponentName name,@NonNull IBinder service) {
            mBinderPool = IBinderPool.Stub.asInterface(service);
            try {
                mBinderPool.asBinder().linkToDeath(mBinderPoolDeathRecipient, 0);
            } catch (RemoteException e) {
                e.printStackTrace();
            } finally {
                mConnectBinderPoolCountDownLatch.countDown();
            }
        }

        @Override
        public void onServiceDisconnected(@Nullable ComponentName name) {
        }
    };

    private IBinder.DeathRecipient mBinderPoolDeathRecipient = new IBinder.DeathRecipient() {
        @Override
        public void binderDied() {
            mBinderPool.asBinder().unlinkToDeath(mBinderPoolDeathRecipient, 0);
            mBinderPool = null;
            connectBinderPoolService();
        }
    };
}

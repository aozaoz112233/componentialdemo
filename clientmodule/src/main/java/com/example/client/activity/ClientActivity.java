package com.example.client.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatButton;
import android.text.TextUtils;
import android.view.View;
import android.widget.Toast;

import com.example.client.R;
import com.example.client.intentservice.BinderpoolIntentService;
/**
 *  描述: 简单的客户端Activity，可使用Remote服务端的服务
 *  @Author: kehua.ding
 *  @Maintainer: kehua.ding
 *  @Date: 2018/11/27
 *  @Copyright: 2018 www.xgimi.com Inc. All rights reserved.
 */
public class ClientActivity extends AppCompatActivity {

    private TextInputEditText mPlaintext;

    private TextInputEditText mComputeFirstText;

    private TextInputEditText mComputeSecondText;

    private ResultBroadcastReceiver mResultBroadcastReceiver;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_client);
        initResultBroadcastReceiver();
        initUi();
    }

    private void initResultBroadcastReceiver() {
        mResultBroadcastReceiver = new ResultBroadcastReceiver();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(BinderpoolIntentService.ONBPRESULT);
        registerReceiver(mResultBroadcastReceiver, intentFilter);
    }

    private void initUi() {
        mPlaintext = findViewById(R.id.textInput_plaintext);
        AppCompatButton mEncrypButton = findViewById(R.id.button_encrypt);
        mEncrypButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(@NonNull View v) {
                if (!TextUtils.isEmpty(mPlaintext.getText())) {
                    Intent intent = new Intent(ClientActivity.this, BinderpoolIntentService.class);
                    intent.setAction(BinderpoolIntentService.SECURITYCODE);
                    intent.putExtra(BinderpoolIntentService.PLAINTEXT, mPlaintext.getText().toString());
                    startService(intent);
                }
            }
        });
        mComputeFirstText = findViewById(R.id.textInput_computeFirst);
        mComputeSecondText = findViewById(R.id.textInput_computeSecond);
        AppCompatButton mComputeButton = findViewById(R.id.button_compute);
        mComputeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(@NonNull View v) {
                if (!(TextUtils.isEmpty(mComputeFirstText.getText()) && TextUtils.isEmpty(
                        mComputeSecondText.getText()))) {
                    Intent intent = new Intent(ClientActivity.this, BinderpoolIntentService.class);
                    intent.setAction(BinderpoolIntentService.COMPUTECODE);
                    intent.putExtra(BinderpoolIntentService.COMPUTEFIRST,
                            Integer.valueOf(mComputeFirstText.getText().toString()));
                    intent.putExtra(BinderpoolIntentService.COMPUTESECOND,
                            Integer.valueOf(mComputeSecondText.getText().toString()));
                    startService(intent);
                }
            }
        });
    }

    private class ResultBroadcastReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(@NonNull Context context, @NonNull Intent intent) {
            if (intent.getAction().equals(BinderpoolIntentService.ONBPRESULT)) {
                String result = intent.getStringExtra(BinderpoolIntentService.ONBPRESULT);
                Toast.makeText(context, result, Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    protected void onDestroy() {
        unregisterReceiver(mResultBroadcastReceiver);
        super.onDestroy();
    }
}

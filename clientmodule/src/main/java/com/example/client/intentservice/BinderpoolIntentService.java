package com.example.client.intentservice;

import android.app.IntentService;
import android.content.Intent;
import android.content.Context;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.example.client.pooling.BinderPool;
import com.example.servicemodule.ICompute;
import com.example.servicemodule.ISecurityCenter;
/**
 *  描述: 用于异步连接Service服务的IntentService，通过广播回调
 *  @Author: kehua.ding
 *  @Maintainer: kehua.ding
 *  @Date: 2018/11/27
 *  @Copyright: 2018 www.xgimi.com Inc. All rights reserved.
 */
public class BinderpoolIntentService extends IntentService {

    private static final String BINDERPOOLTHREAD = "BINDERPOOLTHREAD";

    public static final int BINDER_SECURITY_CENTER = 1;

    public static final int BINDER_COMPUTE = 0;

    public static final String SECURITYCODE = "getSecurityBinder";

    public static final String COMPUTECODE = "getComputeBinder";

    public static final String PLAINTEXT = "plaintext";

    public static final String COMPUTEFIRST = "computeFirst";

    public static final String COMPUTESECOND = "computeSecond";

    public static final String ONBPRESULT = "onBPResult";

    private static final int DEFAULTVALUE = 0;

    public BinderpoolIntentService() {
        super(BINDERPOOLTHREAD);
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    private void securityCenter(@NonNull Intent intent) {
        BinderPool binderPool = BinderPool.getInstance(BinderpoolIntentService.this);
        IBinder securityBinder = binderPool.queryBinder(BINDER_SECURITY_CENTER);
        ISecurityCenter mSecurityCenter = ISecurityCenter.Stub.asInterface(securityBinder);
        try {
            String plaintext = intent.getStringExtra(PLAINTEXT);
            String ciphetext = mSecurityCenter.encrypt(plaintext);
            onFinish(ciphetext);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    private void compute(@NonNull Intent intent) {
        BinderPool binderPool = BinderPool.getInstance(BinderpoolIntentService.this);
        IBinder computeBinder = binderPool.queryBinder(BINDER_COMPUTE);
        ICompute mCompute = ICompute.Stub.asInterface(computeBinder);
        try {
            int result = mCompute.add(intent.getIntExtra(COMPUTEFIRST,DEFAULTVALUE), intent.getIntExtra(COMPUTESECOND,DEFAULTVALUE));
            onFinish(String.valueOf(result));

        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    private void onFinish(@NonNull String result) {
        Intent intent = new Intent();
        intent.setAction(ONBPRESULT);
        intent.putExtra(ONBPRESULT, result);
        sendBroadcast(intent);
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        switch (intent.getAction()) {
            case SECURITYCODE:
                securityCenter(intent);
                break;
            case COMPUTECODE:
                compute(intent);
                break;
            default:
        }
    }

}
